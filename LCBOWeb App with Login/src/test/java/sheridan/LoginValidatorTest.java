package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	// Login name must have alpha characters and numbers only, but should not start with a number
	@Test
	public void testIsValidCharactersAndNotStartWithNumberRegular() {
		assertTrue("Invalid login" , LoginValidator.isValidCharactersAndNotStartWithNumber( "sd1" ) );
	}
	
	@Test
	public void testIsValidCharactersAndNotStartWithNumberException( ) {
		assertFalse("Invalid login" , LoginValidator.isValidCharactersAndNotStartWithNumber( "6sd" ) );
	}
	
	@Test
	public void testIsValidCharactersAndNotStartWithNumberBoundaryIn() {
		assertTrue("Invalid login" , LoginValidator.isValidCharactersAndNotStartWithNumber( "s1d" ) );
	}
	
	@Test
	public void testIsValidCharactersAndNotStartWithNumberBoundaryOut() {
		assertFalse("Invalid login" , LoginValidator.isValidCharactersAndNotStartWithNumber( "s##d" ) );
	}
	
	
	// Login name must have at least 6 alphanumeric characters
	@Test
	public void testIsValidLengthRegular() {
		assertTrue("Invalid login" , LoginValidator.isValidLength( "sarthakdhingra" ) );
	}
	
	@Test
	public void testIsValidLengthException() {
		assertFalse("Invalid login" , LoginValidator.isValidLength( "sd" ) );
	}
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		assertTrue("Invalid login" , LoginValidator.isValidLength( "sartha" ) );
	}
	
	@Test
	public void testIsValidLengthBoundaryOut() {
		assertFalse("Invalid login" , LoginValidator.isValidLength( "sarth" ) );
	}
}
