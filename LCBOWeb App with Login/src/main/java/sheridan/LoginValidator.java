package sheridan;

public class LoginValidator {

	
	// Login name must have alpha characters and numbers only, but should not start with a number
	// Login name must have at least 6 alphanumeric characters
	public static boolean isValidLoginName( String loginName ) {
		return isValidCharactersAndNotStartWithNumber(loginName) && isValidLength(loginName);
	}
	
	// Login name must have alpha characters and numbers only, but should not start with a number
	public static boolean isValidCharactersAndNotStartWithNumber(String loginName) {
		

		boolean checkChars = true;
		for (char c : loginName.toCharArray()) {
			if (!Character.isLetterOrDigit(c)) {
				checkChars = false;
				break;
			}
		
		}

		return checkChars && Character.isLetter(loginName.charAt(0));
	}
	
	// Login name must have at least 6 alphanumeric characters
	public static boolean isValidLength(String loginName) {
		
		int count = 0;
		boolean checkCount = false;
		for (char c : loginName.toCharArray()) {
			if (Character.isLetter(c)) {
				count++;
			}
			if (count == 6) {
				checkCount = true;
				break;
			}
		}
		return checkCount;
		
		
	}
	
	
	
}
